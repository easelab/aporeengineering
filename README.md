# Online Appendix, Re-Engineering of Apo-Games #

### Resources ###

* Main publication: Jacob Krüger and Thorsten Berger. Activities and Costs of Re-Engineering Cloned Variants into an Integrated Platform. In International Working Conference on Variability Modelling of Software-Intensive Systems, VaMoS, pages 21:1–10. ACM, 2020.
	* [Preprint](https://wwwiti.cs.uni-magdeburg.de/iti_db/publikationen/ps/auto/KruegerBVaMoS20:Activities.pdf)
* Resulting product line, Java-based applications re-engineered into compositional product line
    * [Bitbucket repo](https://bitbucket.org/Jacob_Krueger/splc2019_featurehouse_apo-games_spl)
	* [Master thesis](https://odr.chalmers.se/bitstream/20.500.12380/300150/1/CSE%2019-44%20%C3%85kesson%20Nilsson.pdf)
	* [SPLC Challenge Solution Preprint](https://wwwiti.cs.uni-magdeburg.de/iti_db/publikationen/ps/auto/DebbicheLK+2019:SPLC:MigratingJavaApogames.pdf)
* Resulting product line, Android applications re-engineered into annotative product line
    * [Bitbucket repo](https://bitbucket.org/Jacob_Krueger/splc2019_antenna_apo-games_spl)
	* [Master thesis](https://gupea.ub.gu.se/bitstream/2077/62350/1/gupea_2077_62350_1.pdf)
	* [SPLC Challenge Solution Preprint](https://wwwiti.cs.uni-magdeburg.de/iti_db/publikationen/ps/auto/AkessonNK+2019:SPLC:AndroidApoGamesMigration.pdf)
